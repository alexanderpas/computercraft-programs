-- offer all functionality of regular FS API.

function list(path)
  return fs.list(path)
end

function exists(path)
  return fs.exists(path)
end

function isDir(path)
  return fs.isDir(path)
end

function isReadOnly(path)
  return fs.isReadOnly(path)
end

function getName(path)
  return fs.getName(path)
end

function getDrive(path)
  return fs.getDrive(path)
end

function getSize(path)
  return fs.getSize(path)
end

function getFreeSpace(path)
  return fs.getFreeSpace(path)
end

function makeDir(path)
  return fs.makeDir(path)
end

function move(fromPath, toPath)
  return fs.move(fromPath, toPath)
end

function copy(fromPath, toPath)
  return fs.copy(fromPath, toPath)
end

function delete(path)
  return fs.delete(path)
end

function combine(basePath, localPath)
  return fs.combine(basePath, localPath)
end

function open(path, mode)
  return fs.open(path, mode)
end

function find(wildcard)
  return fs.find(wildcard)
end

function getDir(path)
  return fs.getDir(path)
end

function fs.complete(partialName, path, includeFiles, includeSlashes)
  local includeFiles = includeFiles or nil
  local includeSlashes = includeSlashes or nil
  return fs.complete(partialName, path, includeFiles, includeSlashes)
end

-- new functionality.

function saveFile(filename, data)
  local file = io.open(filename, 'w')
  file:write(data.readAll())
  file:close()
  return nil
end

