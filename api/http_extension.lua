if not http then
  error('HTTP Functionality not Availble')
end

os.loadAPI('/api/fs_ext')

-- offer all functionality of regular HTTP API.

function request(url, postData, headers)
  local postData = postData or nil
  local headers = headers or nil
  return http.request(url, postData, headers)
end

function get(url, headers)
  local headers = headers or nil
  return http.get(url, headers)
end

function post(url, postData, headers)
  local headers = headers or nil
  return http.post(url, postData, headers)
end

function checkURL(url)
  return http.checkURL(url)
end

-- new functionality.

function download(url)
  http_ext.request(url)

  while true do
    local event, p1, p2, p3, p4, p5 = os.pullEvent()

    if 'http_success' == event then
      return p2
    elseif 'http_failure' == event then
      return nil
    end
  end
end

function save(url, filename)
  local data = http_ext.download(url)

  if data then
    fs_ext.saveFile(filename, data)
    return true
  else
    return false
  end
end

